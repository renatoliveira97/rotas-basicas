from flask import Flask
from datetime import datetime
from environs import Env

env = Env()
env.read_env()

app = Flask(__name__)

@app.route('/')
def home():
    return {"data": "Hello Flask!"}, 200

@app.route('/current_datetime')
def current_datetime():
    current_datetime = datetime.now().strftime("%d/%m/%Y %H:%M:%S %p")
    current_hour = datetime.now().hour
    message = "Boa noite!"

    if current_hour < 12:
        message = "Bom dia!"

    elif current_hour < 18:
        message = "Boa tarde!"    

    return {"current_datetime": current_datetime, "message": message}, 200
